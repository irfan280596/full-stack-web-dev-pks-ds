<?php
// 
trait Hewan {
  public $nama;    
  public $darah= 50;
  public $jumlahKaki;
  public $keahlian;

  public function atraksi() {
    return "harimau_1 sedang lari cepat ";
  }
}
trait Fight {
    public $attackPower;    
    public $defencePower;
    
    public function serang(){
        return "harimau_1 sedang menyerang elang_3";
    }
    public function diserang() {
      return "harimau_1 sedang diserang";
    }
  }
// Child classes
class Elang {
    use Hewan, Fight;
  public function  getInfoHewan() : string {
    return " jumlahKaki bernilai 2, dan keahlian bernilai terbang tinggi, attackPower = 10 , deffencePower = 5";
  }
}

class Harimau {
    use Hewan, Fight;
  public function getInfoHewan() : string {
    return "maka jumlahKaki bernilai 4, dan keahlian bernilai lari cepat , attackPower = 7 , deffencePower = 8!";
  }
}


// Create objects from the child classes
$obj = new Elang();
echo $obj->getInfoHewan();
echo "<br>";

$obj = new Harimau();
echo $obj->getInfoHewan();
echo "<br>";


?>